/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyecto;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Yisus
 */
public class ListPregunta {
    
   ArrayList<Pregunta> listapregunta = new ArrayList();
   
  
    public void addPregunta(Pregunta p){
        listapregunta.add(p);
    }
    
    
    
   
    public void mostrarPreguntas(int i){
            
            System.out.println("NIVEL: "+listapregunta.get(i).getDificultad()+"   CATEGORIA: "+listapregunta.get(i).getCategoria());   
            System.out.println("¿"+listapregunta.get(i).getPregunta()+"?");
             System.out.println("1)"+ listapregunta.get(i).getOpcion1());
             System.out.println("2)"+listapregunta.get(i).getOpcion2());
             System.out.println("3)"+listapregunta.get(i).getOpcion3());
             System.out.println("4)"+listapregunta.get(i).getOpcion4());
              System.out.print("5)"+"ABANDONAR JUEGO");
             
        
        
       
    }
    
    
     public boolean verificarRespuesta(int i) {
        
        System.out.println("");
        int contestada =  Integer.parseInt(JOptionPane.showInputDialog("ingrese la respuesta"));
        int correcta  = Integer.parseInt(listapregunta.get(i).getRespuestaCorrecta());
        
        if(contestada==5){
           System.out.println("\n----------ABANDONO EL JUEGO---------------"+"\n\n");
           return false;
        }
        
        if(contestada == correcta ){
          
           System.out.println("RESPUESTA CORRECTA"+"\n\n");
           return true;
           
        }else{
             
          System.out.println("Respuesta Incorrecta"+" -- Correcta: "+correcta+"--contestada: "+contestada+"\n\n");
          return false;
          
        }
        
    }
     
     
     
     
     public int escogerPregunta(int i){
       
      Random numAleatorio = new Random();
    
     int value = 0;    
      
       switch(i){
         
             case 1: value = numAleatorio.nextInt(4-0+1) + 0;
                    break;
             case 2: value = numAleatorio.nextInt(9-5+1) + 5;
                    break;
             case 3: value = numAleatorio.nextInt(14-10+1) + 10;
                   break;
             case 4:value = numAleatorio.nextInt(19-15+1) + 15;
                    break;
             case 5:value = numAleatorio.nextInt(24-20+1) + 20;
                   break;
         }
         
       return value;
        
     }
     
     
     public int sumarPuntos(int i){
         
     int value = 0;    
      
       switch(i){
         
             case 1: value = 10;
                    break;
             case 2: value = 20;
                    break;
             case 3: value = 40;
                   break;
             case 4:value = 60;
                    break;
             case 5:value = 80;
                   break;
         }

       return value;
         
     }
       
   
   
    
}
