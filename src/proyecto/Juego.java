/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyecto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Yisus
 */
public class Juego {

    public ListPregunta cargarPreguntas() {
        ListPregunta listpregunta = new ListPregunta();
        try {

            File archivo = new File("preguntas.txt");
            FileReader fr = new FileReader(archivo);
            BufferedReader br = new BufferedReader(fr);

            String linea = "";

            while ((linea = br.readLine()) != null) {

                String[] archivotxt;
                archivotxt = linea.split(",");

                String pregunta = archivotxt[0];
                String categoria = archivotxt[1];
                String nivel = archivotxt[2];
                String opcion1 = archivotxt[3];
                String opcion2 = archivotxt[4];
                String opcion3 = archivotxt[5];
                String opcion4 = archivotxt[6];
                String opcionCorrecta = archivotxt[7];

                Pregunta p = new Pregunta(pregunta, categoria, nivel, opcion1, opcion2, opcion3, opcion4, opcionCorrecta);

                listpregunta.addPregunta(p);

            }

        } catch (Exception e) {
        }

        return listpregunta;

    }

    public void empezarJuego(ListPregunta listpregunta) {

        int acumulado = 0;
        int contador = 1;
        int numPregunta = 0;
        boolean bandera = true;

        Jugador jugador = new Jugador();

        String nombreUsuario = JOptionPane.showInputDialog("Ingrese el nombre del Usuario: ");

        jugador.setNombre(nombreUsuario);

        while (bandera) {

            numPregunta = listpregunta.escogerPregunta(contador);

            listpregunta.mostrarPreguntas(numPregunta);

            bandera = listpregunta.verificarRespuesta(numPregunta);

            if (bandera == true) {
                acumulado = listpregunta.sumarPuntos(contador) + acumulado;
                jugador.setPuntaje(acumulado);
            }

            if (bandera == false) {
                System.out.println("SE TERMINO EL JUEGO");
                jugador.guardarJugador(jugador);
                jugador.mostrarJugadores();

                break;
            }

            contador++;

            if (contador == 6) {
                System.out.println("---------GANOOOOOOO--------------");
                jugador.guardarJugador(jugador);
                jugador.mostrarJugadores();
                break;
            }
        }

    }

}
