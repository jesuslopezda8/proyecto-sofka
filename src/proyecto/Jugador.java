/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyecto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yisus
 */
public class Jugador {

    private String nombre;
    private int puntaje;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    public Jugador(String nombre, int puntaje) {
        this.nombre = nombre;
        this.puntaje = puntaje;
    }

    public Jugador() {
        this.nombre = nombre;
        this.puntaje = puntaje;
    }

    public void guardarJugador(Jugador j) {
        System.out.println("JUGADOR ACTUAL");
        System.out.println("Usuario: " + j.getNombre());
        System.out.println("Puntaje: " + j.getPuntaje());
        System.out.println("\n\n");

        try {
            FileWriter save1 = new FileWriter("datosUsuario.txt", true);
            PrintWriter printtxt1 = new PrintWriter(save1);

            printtxt1.print(j.getNombre());
            printtxt1.write(",");
            printtxt1.println(j.getPuntaje());

            printtxt1.close();

        } catch (IOException ex) {
            Logger.getLogger(Jugador.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void mostrarJugadores() {
        try {

            File archivo = new File("datosUsuario.txt");
            FileReader fr = new FileReader(archivo);
            BufferedReader br = new BufferedReader(fr);

            String linea = "";

            while ((linea = br.readLine()) != null) {

                String[] archivotxt;
                archivotxt = linea.split(",");

                System.out.println("######################################");
                System.out.println("    HISTORIAL DE JUGADORES");
                String nombreUsurio = archivotxt[0];
                String Puntaje = archivotxt[1];

                System.out.println("Jugador: " + nombreUsurio);
                System.out.println("Puntaje: " + Puntaje);

            }

        } catch (Exception e) {
        }
    }

}
