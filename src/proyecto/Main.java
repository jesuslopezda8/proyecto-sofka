/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyecto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JOptionPane;

/**
 *
 * @author Yisus
 */
public class Main {

    public static void main(String[] args) {
      
        ListPregunta listpregunta = new ListPregunta();
        Juego juego = new Juego();
        
        listpregunta = juego.cargarPreguntas();
        
        juego.empezarJuego(listpregunta);
        

    }
    
   
    
    
}
